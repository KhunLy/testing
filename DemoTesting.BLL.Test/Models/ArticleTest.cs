﻿using DemoTesting.BLL.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoTesting.BLL.Test.Models
{
    [TestClass]
    public class ArticleTest
    {
        [TestMethod]
        public void SetNameTest()
        {
            Article a = new Article();
            // Set Name 
            a.Name = "CoCa";
            // Get Name
            Assert.AreEqual("coca", a.Name);
        }

        [TestMethod]
        public void SetPriceTest()
        {
            Article a = new Article();
            a.Price = 42.42;
            Assert.AreEqual(42.42, a.Price);
        }

        [TestMethod]
        public void SetPriceWithNegativeValue()
        {
            Article a = new Article();
            Assert.ThrowsException<ArgumentOutOfRangeException>(
                () => { a.Price = -42.42; }
            );
        }

        [TestMethod]
        public void SetDiscount()
        {
            Article a = new Article();
            a.Discount = 50;
            Assert.AreEqual(50, a.Discount);
        }

        [TestMethod]
        public void SetDiscountWithNegativeValue()
        {
            Article a = new Article();
            Assert.ThrowsException<ArgumentOutOfRangeException>(
                () => { a.Discount = -42; }
            );
        }

        [TestMethod]
        public void SetDiscountWithValueOver100()
        {
            Article a = new Article();
            Assert.ThrowsException<ArgumentOutOfRangeException>(
                () => { a.Discount = 142; }
            );
        }

        [TestMethod]
        public void GetPriceWithDiscount()
        {
            Article a = new Article();
            a.Price = 100;
            a.Discount = 42;
            Assert.AreEqual(58, a.PriceWithDiscount);
        }

        [TestMethod]
        public void GetPriceWithoutDiscount()
        {
            Article a = new Article();
            a.Price = 100;
            a.Discount = null;
            Assert.AreEqual(100, a.PriceWithDiscount);
        }
    }
}
