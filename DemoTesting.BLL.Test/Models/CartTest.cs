﻿using DemoTesting.BLL.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoTesting.BLL.Test.Models
{
    [TestClass]
    public class CartTest
    {
        public Cart c;
        [TestInitialize]
        public void Setup()
        {
            c = new Cart();
            c.Add(new Article() { Name = "Coca", Price = 1, Discount = null});
            c.Add(new Article() { Name = "Fanta", Price = 1.1, Discount = 30});
            c.Add(new Article() { Name = "Dr Pepper", Price = 1.3, Discount = 10});
            c.Add(new Article() { Name = "Nalu", Price = 1.2, Discount = 10});
        }

        [TestMethod]
        public void GetCountTest()
        {
            Assert.AreEqual(4, c.Count);
        }

        [TestMethod]
        public void AddArticleTest()
        {
 
            // vérifier si produit dans le panier
            int cDepart = c.Count;
            c.Add(new Article { Name = "Sprite" });
            Assert.AreEqual(cDepart + 1,  c.Count);
        }

        [TestMethod]
        public void AddExistingArticleTest()
        {
            // vérifier si produit dans le panier
            int cDepart = c.Count;
            Assert.ThrowsException<ArgumentException>(() =>
            {
                c.Add(new Article { Name = "Nalu" });
            });
        }

        [TestMethod]
        public void RemoveArticleTest()
        {

            // vérifier si produit dans le panier
            int cDepart = c.Count;
            c.Remove("coca" );
            Assert.AreEqual(cDepart - 1, c.Count);
        }

        [TestMethod]
        public void AddNonExistingArticleTest()
        {
            // vérifier si produit dans le panier
            int cDepart = c.Count;
            bool reponse = c.Remove("sprite");
            Assert.IsFalse(reponse);
        }

        [TestMethod]
        public void GetTotalLessThan5Articles()
        {
            Assert.AreEqual(4.02, c.GetTotal());
        }

        [TestMethod]
        public void GetTotalMoreThan5Articles()
        {
            c.Add(new Article { Name = "Sprite", Price = 0.98 });
            Assert.AreEqual(4.5, c.GetTotal());
        }
    }
}
