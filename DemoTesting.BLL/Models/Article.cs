﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoTesting.BLL.Models
{
    public class Article
    {

        private string _name;
        public string Name 
        { 
            get
            {
                return _name;
            }
            set 
            {
                _name = value.ToLower();
            }
        }

        private double _price;

        public double Price
        {
            get { return _price; }
            set 
            { 
                if(value < 0)
                {
                    throw new ArgumentOutOfRangeException();
                }
                _price = value; 
            }
        }

        private int? _discount;

        public int? Discount
        {
            get { return _discount; }
            set 
            {
                if((value ?? 0) < 0 || (value ?? 0) > 100)
                {
                    throw new ArgumentOutOfRangeException();
                }
                _discount = value; 
            }
        }

        public double PriceWithDiscount
        {
            get 
            {
                double reduction = Price * (Discount ?? 0) / 100;
                return Price - reduction; 
            }
        }

    }
}
