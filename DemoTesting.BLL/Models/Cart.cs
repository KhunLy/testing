﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoTesting.BLL.Models
{
    public class Cart
    {
        private List<Article> _articles = new List<Article>();

        public int Count
        {
            get
            {
                return _articles.Count;
            }
        }

        public void Add(Article a)
        {
            foreach(Article item in _articles)
            {
                if(a.Name == item.Name)
                {
                    throw new ArgumentException();
                }
            }
            _articles.Add(a);
        }

        public bool Remove(string nom)
        {
            Article toDelete = null;
            foreach (Article item in _articles)
            {
                if (nom == item.Name)
                {
                    toDelete = item;
                }
            }
            if (toDelete == null) return false;
            else
            {
                _articles.Remove(toDelete);
                return true;
            }
        }

        public double GetTotal()
        {
            double somme = 0;
            foreach(Article item in _articles)
            {
                somme += item.PriceWithDiscount;
            }
            return (Count < 5) ? somme : somme - (somme / 10);
        }
    }
}
